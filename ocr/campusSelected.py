#Imports
import cv2 as cv
import numpy as np

def campusSelected(mat):
    #Arguments:
        # mat: pass the online form in once it is read in

    #slices out campus selection area of the member form
    
    campuses = mat[647:790, 120:2400]
    campuses = cv.GaussianBlur(campuses, (7,7), 1)

    #locations of campus bubbles on the member form
    campus_loc = [
    {'campus': 'UCB', 'x1': 28, 'x2': 83, 'y1': 17, 'y2': 72},
    {'campus': 'LBNL', 'x1': 26, 'x2': 83, 'y1': 72, 'y2': 129},
    {'campus': 'UCSC', 'x1': 325, 'x2': 382, 'y1': 9, 'y2': 66},
    {'campus': 'UCOP', 'x1': 324, 'x2': 381, 'y1': 71, 'y2': 129},
    {'campus': 'UCSF', 'x1': 716, 'x2': 774, 'y1': 9, 'y2': 68},
    {'campus': 'UCR', 'x1': 714, 'x2': 771, 'y1': 71, 'y2': 129},
    {'campus': 'UCD', 'x1': 1145, 'x2': 1203, 'y1': 9, 'y2': 67},
    {'campus': 'UCI', 'x1': 1143, 'x2': 1201, 'y1': 70, 'y2': 128},
    {'campus': 'UCSB', 'x1': 1383, 'x2': 1442, 'y1': 10, 'y2': 70},
    {'campus': 'UCLA', 'x1': 1380, 'x2': 1440, 'y1': 71, 'y2': 131},
    {'campus': 'UCM', 'x1': 1829, 'x2': 1888, 'y1': 10, 'y2': 68},
    {'campus': 'UCSD', 'x1': 1828, 'x2': 1887, 'y1': 72, 'y2': 129},
    ]

    #finds the bubble that has been selected
    circles = cv.HoughCircles(campuses, cv.HOUGH_GRADIENT, 1, 20, param1=50, param2=32, minRadius=12, maxRadius=15)
    circles = np.uint16(np.around(circles))
    
    if len(circles[0]) == 1:
        x, y, _ = circles[0][0]
        for i in campus_loc:
            if i['x1'] < x < i['x2'] and i['y1'] < y < i['y2']:
                return i['campus']
    else:
        return "Cannot determine campus"
    








