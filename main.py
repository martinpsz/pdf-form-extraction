from formRetriever import formRetriever
import os
from ocr.ocrPDF import ocrPDF
from shutil import rmtree
import pandas as pd
import pygsheets


def main():
    #Pipeline:

    #1. Retrieve emails from watched inbox and send to temp folder
    formRetriever()

    #2. Loop through temp folder, create images of each pdf, retrieve data and return a list
    form_data = []
    if os.path.exists('temp'):
        for i in os.listdir('temp/'):
            form_data.append(ocrPDF(f'temp/{i}'))

    #3. Create pandas dataframe from dictionary
    df = pd.DataFrame(form_data)

    #4. Send data to google sheet:
    gc = pygsheets.authorize(service_file='refreshing-rune-360417-d78c7a52005a.json')
    sh = gc.open_by_key(os.getenv('SHEET'))
    wks = sh.sheet1
    wks.set_dataframe(df, 'A1')

    #3. Clean up temp folder and images folders:
    #rmtree('temp')
    #rmtree('images')

if __name__ == "__main__":
    main()