import os
from PIL import Image
import pytesseract
from pdf2image import convert_from_path
import cv2 as cv
import numpy as np
from ocr.campusSelected import campusSelected


#ROIs:


'''
NAME = [818:890, 175:1225]
STREET = [956:1017, 175:1225]
CITY = [1076:1142, 175:1225]
ORGANIZER = [1200:1265, 175:1225]
EEID = [820:892, 1380:1890]
TITLE = [820:892, 1890:2377]
WORK_PH = [950:1016, 1380:1890]
HOME_PH = [950:1016, 1890:2377]
WORK_LOC = [1072:1140, 1380:2377]
EMAIL = [1196:1265, 1380:1920]
CELL = [1196:1265, 1950:2377]
SIGNATURE = [1894:1937, 150:521]
SIGNED_ON = [1894:1970, 1553:1937]
'''

def ocrPDF(pdf:str):
    if not os.path.exists('images'):
        os.mkdir('images')

    #Step 1: Convert pdf to an image and save to images temp dir:
    image:list[str] = convert_from_path(pdf, 
                                        dpi=300, 
                                        first_page=1, 
                                        last_page=1,
                                        grayscale=True)

    #Step 2: Save the first image with the member form as a JPEG:
    image[0].save(os.path.join('images', 'form.jpg'), 'JPEG')
    
    #Step 3: Open image in images folder and extract information wanted
    img = cv.imread('images/form.jpg', 0)
    

    form_data = {}
    #Campus selected:
    form_data['Campus'] = campusSelected(img)

    #Form entries
    form_data['Name'] = pytesseract.image_to_string(img[818:890, 175:1225]).strip()
    form_data['Street Address'] = pytesseract.image_to_string(img[956:1017, 175:1225]).strip()
    form_data['City/State'] = pytesseract.image_to_string(img[1076:1142, 175:1225]).strip()
    form_data['Signed By'] = pytesseract.image_to_string(img[1200:1265, 175:1225]).strip()
    form_data['Employee ID'] = pytesseract.image_to_string(img[820:892, 1380:1890]).strip()
    form_data['Job Title'] = pytesseract.image_to_string(img[820:892, 1890:2377]).strip()
    form_data['Work Phone'] = pytesseract.image_to_string(img[950:1016, 1380:1890]).strip()
    form_data['Home Phone'] = pytesseract.image_to_string(img[950:1016, 1890:2377]).strip()
    form_data['Work Location'] = pytesseract.image_to_string(img[1072:1140, 1380:2377]).strip()
    form_data['Email'] = pytesseract.image_to_string(img[1196:1265, 1380:1920]).strip()
    form_data['Cell Phone'] = pytesseract.image_to_string(img[1196:1265, 1950:2377]).strip()
    form_data['Cell Phone'] = pytesseract.image_to_string(img[1196:1265, 1950:2377]).strip()
    form_data['Signed On'] = pytesseract.image_to_string(img[1894:1970, 1553:1937]).strip()
    
    return form_data

