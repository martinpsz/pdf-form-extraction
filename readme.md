This is an automation I put together to monitor a designated inbox. 

The pipeline for how it works is as follows:

- Check designated inbox for messages that contain a specific pdf.
- If the PDF exists, download it a temporary folder.
- Convert the pdfs in the temporary folder into images.
- From each image, using OpenCV, extract the text or selection in the regions of interest
- Convert the list of python dictionaries created with the last step into a pandas dataframe
- Send dataframe to a google spreadsheet.