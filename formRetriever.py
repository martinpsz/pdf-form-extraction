import os
from dotenv import load_dotenv
load_dotenv()
import imaplib
import email
import re

def formRetriever(user:str=f"{os.getenv('USERNAME')}", pswd:str=f"{os.getenv('PSWD')}", server:str=f"{os.getenv('SERVER')}"):
    with imaplib.IMAP4_SSL(server) as mail:
        mail.login(user, pswd)

        
        mail.select('INBOX.Memberships')
        
        _, msgs = mail.search(None, '(UNSEEN SUBJECT "Fwd: UPTE member reaffirmation")')

        if len(msgs[0].split()) != 0:
            if not os.path.isdir('temp'):
                os.mkdir('temp')   
            
            for i in msgs[0].split():
                _, data = mail.fetch(i, '(RFC822)')
                msg = email.message_from_bytes(data[0][1])

                
                for em in msg.walk():
                    if em['Subject'] is not None:
                        subject = re.search(r'and\s(.*)\sis', re.sub(r'\s+', ' ', em['Subject'])).group(1)
                        
                    if em.get_content_disposition() != None:
                        with open(os.path.join('temp', f"{subject}.pdf"), 'wb') as f:
                            f.write(em.get_payload(decode=True))
               
     
        
        
            


